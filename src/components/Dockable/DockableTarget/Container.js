import React, { Component } from "react";

import Context from "../Context";
import DockableTarget from "./DockableTarget";

class DockableTargetContainer extends Component {
  render() {
    const { children } = this.props;

    return (
      <Context.Consumer>
        {context => {
          return <DockableTarget context={context}>{children}</DockableTarget>;
        }}
      </Context.Consumer>
    );
  }
}

export default DockableTargetContainer;
