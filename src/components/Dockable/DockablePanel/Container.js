import React, { Component } from 'react';

import Context from '../Context';
import DockablePanel from './DockablePanel';

class DockablePanelContainer extends Component {
  render() {
    const { children } = this.props;

    return (
      <Context.Consumer>
        {(context) => {
          return (
            <DockablePanel {...this.props} context={context}>
              {children}
            </DockablePanel>
          );
        }}
      </Context.Consumer>
    );
  }
}

export default DockablePanelContainer;
