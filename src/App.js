import React, { Component } from 'react';

import { Button, Wrap } from './styles';
import Dockable from './components/Dockable';
import Timer from './components/Timer';

class App extends Component {
  constructor() {
    super();
    this.aRef = React.createRef();
    this.bRef = React.createRef();

    this.state = {
      centerWidth: 300,
      height: 500,
      snappedTarget: null,
    };
  }

  render() {
    const { centerWidth, height, snappedTarget, snapTargets } = this.state;

    return (
      <Wrap style={{ height, minHeight: 300 }}>
        <Dockable.Provider>
          <Dockable.Target>
            <div style={{ background: '#ddd', flexGrow: 2 }}>Left target</div>
          </Dockable.Target>

          <div style={{ flexGrow: 3, width: centerWidth }}>
            <Button type="button" onClick={() => this.setState({ centerWidth: centerWidth - 10 })}>
              Shrink Width
            </Button>

            <Button type="button" onClick={() => this.setState({ height: height + 10 })}>
              Add Height
            </Button>
          </div>

          <Dockable.Target>
            <div style={{ background: '#ddd', flexGrow: 1 }}>Right target</div>
          </Dockable.Target>

          <Dockable.Panel
            snappedTarget={snappedTarget}
            snapTargets={snapTargets}
            title="Panel 1"
            onTargetDrop={this.handleTargetDrop}
          >
            <Timer />
          </Dockable.Panel>
        </Dockable.Provider>
      </Wrap>
    );
  }
}

export default App;
