import styled from "styled-components";

const Button = styled.button`
  background: #fcf7ff;
  border: 1px solid #c4cad0;
  font-size: 1.2em;
  padding: 5px;
  width: 100%;
`;

const Wrap = styled.div`
  display: flex;
  font-family: "Segoe UI", "Segoe UI Web (West European)", "Segoe UI",
    -apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue", sans-serif;
  font-weight: 200;
  text-align: center;
  width: 100%;
`;

export { Button, Wrap };
